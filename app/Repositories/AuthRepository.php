<?php

namespace App\Repositories;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthRepository {

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    //proses login
    public function login($request)
    {
        $credentials = $request->only('email', 'password');
        $credentials['is_deleted'] = false;

        if(!Auth::attempt($credentials)){
            return response()->json([
                'status' => false,
                'messgae' => 'Login failed, please check your credentials',
            ],401);
        }

        $user = User::where('email', $credentials['email'])
        ->where('is_deleted', false)
        ->first();

        //saat berhasil login, maka token dikirim dalam bentuk json untuk di consume
        $user['token'] = $user->createToken(config('app.name'))->plainTextToken;

        return response()->json([
            'status' => true,
            'messgae' => 'Created Successfully',
            'data' => $user,
        ],200);
    }
}

