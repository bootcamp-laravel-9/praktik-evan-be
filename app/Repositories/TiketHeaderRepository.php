<?php

namespace App\Repositories;

use App\Http\Resources\TiketHeaderResource;
use App\Models\TiketHeader;
use App\Models\User;

class TiketHeaderRepository {

    protected $tiketHeader;

    public function __construct(TiketHeader $tiketHeader)
    {
        $this->tiketHeader = $tiketHeader;
    }

    public function storeOrUpdate($request, $method = null)
    {
        //jika method == 'PUT' maka akan menjalakan update, jika method = null maka akan menjalankan Create
        if($method == 'PUT' && !$this->tiketHeader->find($request->id))
        {
            throw new \Exception("Data User tidak ditemukan.", 400);
        }

        // Generate random 2 characters
        $random_chars = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, 2);
        $random_chars2 = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, 3);

        // Generate ticket number with the desired format
        $no_tiket = 'ARS-' . date('md') . '-' . $random_chars . '-' . $random_chars2;

        $data = $this->tiketHeader->updateOrCreate(
        [
            'id' => $request->id
        ],
        [
            'no_tiket' => $no_tiket,
            'name' => $request->name,
            'email' => $request->email,
            'no_telp' => $request->no_telp,
            'address' => $request->address,
            'date_ticket' => $request->date_ticket
        ]);
        return $data;
    }
    // public function storeOrUpdate($request, $method = null)
    // {
    //     //jika method == 'PUT' maka akan menjalakan update, jika method = null maka akan menjalankan Create
    //     if($method == 'PUT' && !$this->tiketHeader->find($request->id))
    //     {
    //         throw new \Exception("Data User tidak ditemukan.", 400);
    //     }

    //     $data = $this->tiketHeader->updateOrCreate(
    //     [
    //         'id' => $request->id
    //     ],
    //     [
    //         'no_tiket' => $request->no_tiket,
    //         'name' => $request->name,
    //         'email' => $request->email,
    //         'no_telp' => $request->no_telp,
    //         'address' => $request->address,
    //         'date_ticket' => $request->date_ticket
    //     ]);
    //     return $data;
    // }

    public function all($id = null)
    {
        if ($id == null){
            //get by id
            $response = TiketHeaderResource::collection($this->tiketHeader->all());
            return $response;
        }

        $tiketHeader = $this->tiketHeader->find($id);
        if(!$tiketHeader){
            throw new \Exception("Data user tidak ditemukan.", 400);
        }

        $response = new TiketHeaderResource($tiketHeader);
        return $response;
    }

    // public function destroy($id)
    // {
    //     $car = $this->tiketHeader->find($id);

    //     if(!$car){
    //         throw new \Exception("Data Brand tidak ditemukan.", 400);
    //     }
    //     $car->delete();
    // }
}

